#!/usr/bin/env python3
import re
import urllib.request
from bs4 import BeautifulSoup

MAX_JOBS = 10

def main():
    page = urllib.request.urlopen(
        f"https://pnnl.jobs/jobs-in/ajax/joblisting/?sort=date&num_items={MAX_JOBS}").read()
    html = BeautifulSoup(page, "html.parser")
    jobs_list = html.find_all("li", "direct_joblisting with_description")

    jobs_dict = {}
    for job in jobs_list:
        url = "https://pnnl.jobs{}".format(job.find('a').get('href'))
        url_id = re.search(r"[A-F0-9]{32}", url).group(0)
        jobs_dict[url_id] = {
            "title": job.find('span', 'resultHeader').text.strip(),
            "location": job.find('span', 'hiringPlace').text.strip(),
            "url": "https://pnnl.jobs{}".format(job.find('a').get('href')),
        }

    for url_id, job in jobs_dict.items():
        print("Job Title:", job["title"])
        print("Location:", job["location"])
        print("URL:", job["url"])
        print("URL ID:", url_id)
        print()

if __name__ == "__main__":
    main()
