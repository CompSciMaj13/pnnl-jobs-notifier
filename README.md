# PNNL Jobs Notifier

This project makes it easy to automatically query and send email notifications of newly posted PNNL jobs.

## Requirements

* Python3.7
* Pip3

## Setup

It is assumed that this project will be ran in a Linux environment. If using Windows or Mac, you will need to research the similar commands.

### Virtual environment

It is recommended to run out of a python virtual environment.

#### Pipenv

This project supports using Pipenv. To learn more visit https://pipenv.readthedocs.io/en/latest/.

To install the needed requirements:

```bash
pipenv install
```

To enter into the virtual environment:

```bash
pipenv shell
```

To run the script without entering the virtual environment:

```bash
pipenv run ./notify_jobs_cli.py -h
```

#### Venv

To create a venv virtual environment, first create a root directory in the top-level of the project.

```bash
mkdir root
```

Create the virtual environment.

```bash
python -m venv root/
```

Then, activate the virtual environment.

```bash
source root/bin/activate
```

You are now in a sandboxed virtual environment where you can install python packages and not interfere with the root python packages installed on the host system.

To install the required python packages, we will use pip. Inside the top-level of the project, run:

```
pip install -r requirements.txt
```

### Email password

It's recommended to use a one-time app password, preferably one with limited privileges. There are two ways to supply your email password for sending emails.

* Pass the `--enter-pass` flag and manually enter you password at a password prompt (using python's getpass()). This will avoid having to save a password in a text file and will not display the typed password in the terminal.
* Create a text file with the password on the first line and add the file name to the `PW_FILE` global variable in the `notify_jobs_cli.py` script. Be sure to use appropriate permissions to prevent unauthorized reading of the password.

### Email account and to addresses

The sending email account is set by the `SENDING_EMAIL` variable in the script as a string. The to addresses can be set by the `TO_ADDRS` variable in the script as a list of strings.

## Usage

The script can be ran to either print (`-p/--print`) or email (`-e/--email`) the currently found jobs and all new jobs. 

**Script Arguments:**

```
  -h, --help            show this help message and exit
  -p, --print           Print any new jobs to the screen
  -n NUM_OF_JOBS, --num-of-jobs NUM_OF_JOBS
                        Max jobs to have returned. Default: 30
  -t JOB_TITLE, --job-title JOB_TITLE
                        Job title search parameter. Default: ''
  -l LOCATION, --location LOCATION
                        Location search parameter. Default: ''
  -s SEARCH_QUERY, --search-query SEARCH_QUERY
                        SEO search query to refine search results
  --sleep SLEEP         Time in seconds to wait before re-querying. Default:
                        600
  -e, --email           Send emails of new job postings
  --enter-pass          Enter email password in a password prompt. (more
                        secure)
  --debug               Add extra debugging informationl account
```
