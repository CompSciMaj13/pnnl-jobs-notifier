#!/usr/bin/env python3
"""Simple script to be notified of PNNL jobs"""
from typing import Any, Dict

import argparse
import re
import smtplib
import time
import urllib.request
import uuid
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from getpass import getpass

from bs4 import BeautifulSoup
from bs4 import ResultSet

URL_ROOT = "https://pnnl.jobs"
URL_SEARCH = "jobs-in/ajax/joblisting/"

# Email global params
SERVER_EMAIL = ""
TO_ADDRS = ["",]
PW_FILE = ""
PASSWD = ""
QUERY_ID = str(uuid.uuid4())[-8:]

def __parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="CLI tool to notify of new job postings at PNNL")
    parser.add_argument("-p", "--print", help="Print any new jobs to the screen",
                        action="store_true", default=False, required=False)
    parser.add_argument("-n", "--num-of-jobs", help="Max jobs to have returned. Default: 30",
                        type=int, required=False, default=30)
    parser.add_argument("-t", "--job-title", help="Job title search parameter. Default: ''",
                        type=str, required=False, default="")
    parser.add_argument("-l", "--location", help="Location search parameter. Default: ''",
                        type=str, required=False, default="")
    parser.add_argument("-s", "--search-query", help="SEO search query to refine search results",
                        type=str, required=False, default="")
    parser.add_argument("--sleep", help="Time in seconds to wait before re-querying. Default: 600",
                        type=int, required=False, default=600)

    parser.add_argument("-e", "--email", help="Send emails of new job postings", action="store_true",
                        default=False, required=False)
    parser.add_argument("--enter-pass", help="Enter email password in a password prompt. (more secure)",
                        action="store_true", default=False, required=False)
    parser.add_argument("--debug", help="Add extra debugging information", action="store_true",
                        default=False, required=False)

    return parser.parse_args()

def get_jobs_dict(jobs_resultset: ResultSet) -> Dict[str, Dict[str, str]]:
    """Return jobs in a dictionary

    :param jobs_resultset: BeautifulSoup4 ResultSet object
    :type jobs_resultset: ResultSet
    :returns: Dictionary of jobs
    :rtype: Dict[str, Dict[str, str]]
    """
    jobs_dict: Dict[str, Any] = {}
    for job in jobs_resultset:
        job_url = job.find("a")
        job_url["href"] = URL_ROOT + job_url.get("href")
        url_id = re.search(r"[A-F0-9]{32}", job_url.get("href")).group(0)
        snippet = job.find("div", "directseo_jobsnippet")
        if snippet:
            snippet = snippet.text.strip().strip("...").strip()
        if url_id not in jobs_dict:
            jobs_dict[url_id] = {
                "title": job.find("span", "resultHeader").text.strip(),
                "location": job.find("span", "hiringPlace").text.strip(),
                "url": job_url.get("href"),
                "snippet": snippet if snippet is not None else "",
                "html": job,
            }
        else:
            raise IndexError("Dictionary key already exists!")
    return jobs_dict

def get_search_results(num_of_jobs: int, title: str, location: str, squery: str) -> ResultSet:
    """Query for jobs page and return a ResultSet object containing jobs

    :param num_of_jobs: Number of jobs to query for
    :type num_of_jobs: int
    :param title: Job title search parameter
    :type title: str
    :param location: Location search parameter
    :type location: str
    :param squery: SEO search query parameter
    :type squery: str
    :returns: BeautifulSoup4 ResultSet object
    :rtype: bs4.ResultSet
    """
    search_url = f"{URL_ROOT}/{title.replace(' ', '-')}/{URL_SEARCH}?sort=date&num_items={num_of_jobs}&location={location}&q={squery.replace(' ', '+')}"
    page = urllib.request.urlopen(search_url).read()
    html = BeautifulSoup(page, "html.parser")
    return html.find_all("li", "direct_joblisting with_description")

def print_jobs(jobs_dict: Dict[str, Dict[str, str]]) -> None:
    """Prints jobs to the screen

    :param jobs_dict: Dictionary containing jobs
    :type jobs_dict: Dict[str, Dict[str, str]]
    """
    for url_id, job in jobs_dict.items():
        print("URL ID:", url_id)
        print("Job Title:", job["title"])
        print("Location:", job["location"])
        print("URL:", job["url"])
        if job["snippet"]:
            print("Snippet:\n", job["snippet"])
        print()

def get_new_jobs(old_jobs: Dict[str, Dict[str, str]], updated_jobs: Dict[str, Dict[str, str]]) -> Dict[str, Dict[str, str]]:
    """Compare jobs and return new jobs not seen before

    :param old_jobs: Currently known jobs
    :type old_jobs: Dict[str, Dict[str, str]]
    :return: Dictionary with new jobs
    :rtype: Dict[str, Dict[str, str]]
    """
    new_jobs = {}
    for url_id, job in updated_jobs.items():
        if url_id not in old_jobs:
            new_jobs[url_id] = job
    return new_jobs

def send_email(subject: str, message: str) -> str:
    server = __get_server()
    msg = __generate_msg(subject, message)
    try:
        if SERVER_EMAIL and TO_ADDRS:
            server.sendmail(SERVER_EMAIL, TO_ADDRS, msg)
            server.quit()
            return msg
        print("Could not send email! Server email address and/or to addresses not specified.")
    except smtplib.SMTPRecipientsRefused as err:
        print("The following recipients were refused by the server. Please try correct and try again!")
        print(err.recipients)
    except smtplib.SMTPSenderRefused as err:
        print("The server didn't accept the from address {}!".format(err.sender))
        print(err)
    except smtplib.SMTPException as err:
        print("Error while attempting to send email!")
        print(err)
    return ""

def __get_server() -> smtplib.SMTP:
    try:
        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.starttls()
        if SERVER_EMAIL:
            server.login(SERVER_EMAIL, __get_password())
        else:
            print("Server email address not specified! Could not log on to SMTP server")
            exit(1)
        return server
    except smtplib.SMTPConnectError:
        print("Could not connect to the email server! Please check the network connection and try again.")
    except smtplib.SMTPAuthenticationError:
        print("Error while authenticating with email server! Please check credentials and try again.")
    except smtplib.SMTPNotSupportedError as err:
        print("Server does not appear to support SMTP! Please check SMTP support and try again.")
        print(err)
    except smtplib.SMTPException as err:
        print("Error while attempting to log on to the email server!")
        print(err)
    exit(1)

def __get_password() -> str:
    password = ""
    try:
        if PASSWD:
            password = PASSWD
        elif PW_FILE:
            with open(PW_FILE, "r") as passfile:
                password = passfile.readline()
        else:
            print("No password specified!")
            exit(1)
    except FileNotFoundError:
        print("Password file not found!")
        exit(1)
    return password

def __generate_msg(subject: str, message: str) -> str:
    msg = MIMEMultipart()
    if SERVER_EMAIL and TO_ADDRS:
        msg["From"] = SERVER_EMAIL
        msg["To"] = ", ".join(TO_ADDRS)
    else:
        print("Server email address and to addresses are required to send message!")
    msg["Subject"] = subject
    msg.attach(MIMEText(message, "html"))
    return msg.as_string()

def email_jobs(subject: str, jobs_dict: Dict[str, Dict[str, str]], args: argparse.Namespace) -> None:
    """Send an email with given jobs"""
    message = ""
    for job in jobs_dict.values():
        message += str(job["html"])
    if args.debug:
        message += f"<br>Debug script arguments:<br>{str(args)}"
    send_email(subject, message)

def main():
    """Entry point of script"""
    args = __parse_args()

    # Get first job results
    jobs_results = get_search_results(args.num_of_jobs, args.job_title, args.location, args.search_query)
    jobs_dict = get_jobs_dict(jobs_results)

    if args.print:
        print("Currently found jobs:")
        print_jobs(jobs_dict)
        if args.debug:
            print(str(args))

    if args.email:
        if args.enter_pass:
            global PASSWD
            PASSWD = getpass(prompt="Please Enter Email Password: ")
        email_jobs(f"Current PNNL Job Listings - Auto CLI Query ID: {QUERY_ID}", jobs_dict, args)

    # Run loop to check for new job postings
    dots = ""
    while True:
        time.sleep(args.sleep)
        updated_results = get_search_results(args.num_of_jobs, args.job_title, args.location, args.search_query)
        updated_jobs = get_jobs_dict(updated_results)
        new_jobs = get_new_jobs(jobs_dict, updated_jobs)

        if new_jobs:
            dots = ""
            if args.print:
                print(f"{len(new_jobs)} new jobs found!")
                print_jobs(new_jobs)
                if args.debug:
                    print(str(args))
            if args.email:
                email_jobs(f"{len(new_jobs)} new PNNL jobs posted! - Auto CLI Query ID: {QUERY_ID}", new_jobs, args)
            jobs_dict.update(new_jobs)
        else:
            dots += "."
            if args.print:
                print("No new jobs found" + dots, end="\r")

if __name__ == "__main__":
    main()
